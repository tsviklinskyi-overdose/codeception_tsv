<?php

class AddToCartCest
{
    public function AddToCartTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->wait('2');
        $I->waitForElementClickable("//li[contains(@class,'nav-2 category')]//a[@class='level-top']");
        $I->click("//li[contains(@class,'nav-2 category')]//a[@class='level-top']");
        $I->waitForElementVisible("//ol[@class='products list items product-items']");
        $I->wait('3');
        $I->click("(//a[@class='product-item-link'])[2]");
        $I->waitForElementClickable("//button[@id='product-addtocart-button']");
        $I->wait('3');
        $I->click("//button[@id='product-addtocart-button']");
        $I->waitForElementVisible("//span[@class='counter qty _block-content-loading']//div[@class='loader']");
        $I->waitForElementNotVisible("//span[@class='counter qty _block-content-loading']//div[@class='loader']");
        $I->waitForElementVisible("//span[@class='counter-number']");
        $I->see('1', "//span[@class='counter-number']");
        $I->click("//a[@class='action showcart']");
        $I->waitForElement("//button[contains(@id,'plus-cart-item-')]");
        $I->wait('1');
        $I->click("//button[contains(@id,'plus-cart-item-')]");
        $I->waitForElementVisible("//div[@class='block-content _block-content-loading']//div[@class='loader']");
        $I->waitForElementNotVisible("//div[@class='block-content _block-content-loading']//div[@class='loader']");
        $I->waitForElementVisible("//input[contains(@id,'-qty') and @data-item-qty='2']");
        $I->wait('1');
        $I->click("//button[contains(@id,'minus-cart-item-')]");
        $I->waitForElementVisible("//div[@class='block-content _block-content-loading']//div[@class='loader']");
        $I->waitForElementNotVisible("//div[@class='block-content _block-content-loading']//div[@class='loader']");
        $I->waitForElementVisible("//input[contains(@id,'-qty') and @data-item-qty='1']");
        $I->wait('1');
        $I->click("//a[@class='action delete']");
        $I->waitForElementClickable("//button[@class='action primary action-accept']");
        $I->click("//button[@class='action primary action-accept']");
        $I->waitForElementVisible("//div[@class='block-content _block-content-loading']//div[@class='loader']");
        $I->waitForElementNotVisible("//div[@class='block-content _block-content-loading']//div[@class='loader']");
        $I->waitForText('You have no items in your shopping cart.', '15', "//span[@class='subtitle empty']");
        $I->wait('1');
    }
}
