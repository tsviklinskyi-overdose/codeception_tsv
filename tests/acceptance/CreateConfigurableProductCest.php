<?php 

class CreateConfigurableProductCest
{
    public function CreateConfigurableProductTest(AcceptanceTester $I)
    {
        $I->amOnPage('/admin');
        $I->waitForElementVisible("//legend[@class='admin__legend']");
        $I->see('Welcome, please sign in', "//legend[@class='admin__legend']//span[contains(text(),'')]");
        $I->fillField("//input[@id='username']", 'tsv');
        $I->fillField("//input[@id='login']", 'Test1234');
        $I->click("//button[@class='action-login action-primary']");
        $I->waitForText('Dashboard', '30', "//h1[contains(@class,'page-title')]");
        $I->wait('2');
        $I->clickWithLeftButton("//ul[@id='nav']//li[@id='menu-magento-catalog-catalog']/a");
        $I->wait('2');
        $I->click("(//li[@id='menu-magento-catalog-catalog']//li[contains(@class,'level-2')])[1]//a");
        $I->waitForText('Products', "15", "//h1");
        $I->waitForElementVisible("//div[@id='container']//div[@class='spinner']");
        $I->waitForElementNotVisible("//div[@id='container']//div[@class='spinner']");
        $I->wait('2');
        $I->click("//button[@class='action-toggle primary add']");
        $I->click("//span[@title='Configurable Product']");
        $I->waitForText('New Product', '15', "//h1[@class='page-title']");
        $I->waitForElementVisible("//div[@class='admin__form-loading-mask']//div[@class='spinner']");
        $I->waitForElementNotVisible("//div[@class='admin__form-loading-mask']//div[@class='spinner']");
        $I->wait('2');
        $I->fillField("//input[@name='product[sku]' and contains(@id,'')]", 'automation_configurable_sku');
        $I->fillField("//input[@name='product[name]' and contains(@id,'')]", 'Automation Configurable Product');
        $I->fillField("//input[@name='product[price]' and contains(@id,'')]", '100.00');
        $I->click("//div[contains(text(),'Select...')]");
        $I->wait('1');
        $I->click("//label[contains(text(), 'Globes')]");
        $I->click("//div[@class='action-select admin__action-multiselect _active']");
        $I->wait('1');
        $I->clickWithLeftButton("//span[contains(text(),'Create Configurations')]");
        $I->waitForText('color','15', "//div[@class='data-grid-cell-content'][contains(text(),'color')]");
        $I->wait('1');
        $I->click("//input[@id='idscheck93']");
        $I->wait('1');
        $I->click("//button[@class='action-default action-primary action-next-step']");
        $I->waitForElementClickable("//button[@class='action-select-all action-tertiary']");
        $I->click("//button[@class='action-select-all action-tertiary']");
        $I->wait('1');
        $I->click("//button[@class='action-default action-primary action-next-step']");
        $I->wait('1');
        $I->click("//button[@class='action-default action-primary action-next-step']");
        $I->wait('1');
        $I->click("//button[@class='action-default action-primary action-next-step']");
        $I->wait('2');
        $I->fillField("//tr[1]//div[@data-index='price_edit']//input[contains(@id,'')]", '10.00');
        $I->fillField("//tr[1]//div[@data-index='quantity_edit']//input[contains(@id,'')]", '10');
        $I->fillField("//tr[2]//div[@data-index='price_edit']//input[contains(@id,'')]", '15.00');
        $I->fillField("//tr[2]//div[@data-index='quantity_edit']//input[contains(@id,'')]", '15');
        $I->fillField("//tr[3]//div[@data-index='price_edit']//input[contains(@id,'')]", '20.00');
        $I->fillField("//tr[3]//div[@data-index='quantity_edit']//input[contains(@id,'')]", '20');
        $I->wait('2');
        $I->click("//button[@id='save-button']");
        $I->waitForElementVisible("//div[@class='popup popup-loading']");
        $I->waitForElementNotVisible("//div[@class='popup popup-loading']");
        $I->waitForText('You saved the product.', '15', "//div[@class='message message-success success']//div");
        $I->wait('2');
        $I->click("//button[@id='back']");
        $I->waitForElementClickable("//button[contains(text(),'Filters')]");
        $I->wait('5');
        $I->click("//button[contains(text(),'Filters')]");
        $I->fillField("//input[@name='sku']", 'automation_configurable');
        $I->click("//button[@class='action-secondary']");
        $I->waitForText('4','15', "//div[@class='col-xs-3']//div[@class='admin__control-support-text']");
        $I->wait('3');
        $I->click("(//button[@class='action-multicheck-toggle'])[2]");
        $I->click("//div[@class='action-multicheck-wrap _active']//ul[@class='action-menu']//li[1]/span");
        $I->waitForText('(4 selected)','15', "//div[@class='col-xs-3']//div[contains(text(),'')]");
        $I->click("//div[@class='col-xs-2']//button[@class='action-select']");
        $I->click("//div[@class='col-xs-2']//span[contains(text(),'Delete')]");
        $I->waitForElementClickable("//button[@class='action-primary action-accept']");
        $I->click("//button[@class='action-primary action-accept']");
        $I->waitForText('A total of 4 record(s) have been deleted.', '15', "//div[@class='message message-success success']//div[contains(text(),'')]");
        $I->wait('3');
        $I->waitForElementClickable("//div[@class='admin__data-grid-header-row']//button[@class='action-remove']");
        $I->wait('1');
        $I->click("//div[@class='admin__data-grid-header-row']//button[@class='action-remove']");
        $I->waitForElementVisible("//div[@id='container']//div[@class='spinner']");
        $I->waitForElementNotVisible("//div[@id='container']//div[@class='spinner']");
        $I->wait('1');
    }
}
