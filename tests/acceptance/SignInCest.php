<?php

class SignInCest
{

    public function SignInTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->waitForElementVisible("//input[@id='top-email']");
        $I->fillField("//input[@id='top-email']", 'automation-test@mailinator.com');
        $I->fillField("//input[@id='top-pass']", 'Test1234');
        $I->click("//button[@id='top-send2']");
        $I->waitForText('Welcome test', "15", "//span[@class='customer-welcome']");
        $I->click("//a[@id='showloginform-button']");
        $I->seeCurrentUrlEquals('/sales/order/history/');
        $I->wait('2');
        $I->see('My Orders', "//div[@id='account-nav']");
        $I->waitForElementClickable("//li[@class='nav item']//a[contains(text(),'Log out')]");
        $I->click("//li[@class='nav item']//a[contains(text(),'Log out')]");
        $I->waitForElementVisible("//span[@class='base']");
        $I->seeCurrentUrlEquals('/customer/account/logoutSuccess/');
        $I->waitForElementVisible("//div[@class='homepage-category-slider slick-initialized slick-slider']");
    }


}
