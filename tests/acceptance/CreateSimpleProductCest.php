<?php

class CreateSimpleProductCest
{
    public function CreateSimpleProductTest(AcceptanceTester $I)
    {
        $I->amOnPage('/admin');
        $I->waitForElementVisible("//legend[@class='admin__legend']");
        $I->see('Welcome, please sign in', "//legend[@class='admin__legend']//span[contains(text(),'')]");
        $I->fillField("//input[@id='username']", 'tsv');
        $I->fillField("//input[@id='login']", 'Test1234');
        $I->click("//button[@class='action-login action-primary']");
        $I->waitForText('Dashboard', '30', "//h1[contains(@class,'page-title')]");
        $I->wait('2');
        $I->clickWithLeftButton("//ul[@id='nav']//li[@id='menu-magento-catalog-catalog']/a");
        $I->wait('2');
        $I->click("(//li[@id='menu-magento-catalog-catalog']//li[contains(@class,'level-2')])[1]//a");
        $I->waitForText('Products', "15", "//h1");
        $I->waitForElementVisible("//div[@id='container']//div[@class='spinner']");
        $I->waitForElementNotVisible("//div[@id='container']//div[@class='spinner']");
        $I->wait('2');
        $I->click("//button[@class='action-toggle primary add']");
        $I->click("//span[@title='Simple Product']");
        $I->waitForText('New Product', '15', "//h1[@class='page-title']");
        $I->waitForElementVisible("//div[@class='admin__form-loading-mask']//div[@class='spinner']");
        $I->waitForElementNotVisible("//div[@class='admin__form-loading-mask']//div[@class='spinner']");
        $I->wait('2');
        $I->fillField("//input[@name='product[sku]' and contains(@id,'')]", 'automation_product_sku');
        $I->fillField("//input[@name='product[name]' and contains(@id,'')]", 'Automation Product');
        $I->fillField("//input[@name='product[price]' and contains(@id,'')]", '100.00');
        $I->click("//div[contains(text(),'Select...')]");
        $I->wait('1');
        $I->click("//label[contains(text(), 'Globes')]");
        $I->click("//div[@class='action-select admin__action-multiselect _active']");
        $I->wait('1');
        $I->click("//button[@id='save-button']");
        $I->waitForElementVisible("//div[@class='popup popup-loading']");
        $I->waitForElementNotVisible("//div[@class='popup popup-loading']");
        $I->waitForText('You saved the product.', '15', "//div[@class='message message-success success']//div");
        $I->wait('2');
        $I->click("//button[@id='back']");
        $I->waitForElementClickable("//button[contains(text(),'Filters')]");
        $I->wait('5');
        $I->click("//button[contains(text(),'Filters')]");
        $I->fillField("//input[@name='sku']", 'automation');
        $I->click("//button[@class='action-secondary']");
        $I->waitForText('1','15', "//div[@class='col-xs-3']//div[@class='admin__control-support-text']");
        $I->wait('3');
        $I->click("//input[contains(@id,'idscheck')]");
        $I->click("//div[@class='col-xs-2']//button[@class='action-select']");
        $I->click("//div[@class='col-xs-2']//span[contains(text(),'Delete')]");
        $I->waitForElementClickable("//button[@class='action-primary action-accept']");
        $I->click("//button[@class='action-primary action-accept']");
        $I->waitForText('A total of 1 record(s) have been deleted.', '15', "//div[@class='message message-success success']//div[contains(text(),'')]");
        $I->wait('3');
        $I->waitForElementClickable("//div[@class='admin__data-grid-header-row']//button[@class='action-remove']");
        $I->wait('1');
        $I->click("//div[@class='admin__data-grid-header-row']//button[@class='action-remove']");
        $I->waitForElementVisible("//div[@id='container']//div[@class='spinner']");
        $I->waitForElementNotVisible("//div[@id='container']//div[@class='spinner']");
        $I->wait('1');
    }
}
