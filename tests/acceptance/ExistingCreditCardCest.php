<?php 

class ExistingCreditCardCest
{
    public function ExistingCreditCardTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->waitForElementVisible("//input[@id='top-email']");
        $I->fillField("//input[@id='top-email']", 'automation-test@mailinator.com');
        $I->fillField("//input[@id='top-pass']", 'Test1234');
        $I->click("//button[@id='top-send2']");
        $I->waitForText('Welcome test', "15", "//span[@class='customer-welcome']");
        $I->wait('2');
        $I->click("//li[contains(@class,'nav-2 category')]//a[@class='level-top']");
        $I->waitForElementVisible("//ol[@class='products list items product-items']");
        $I->wait('3');
        $I->click("(//a[@class='product-item-link'])[2]");
        $I->waitForElementClickable("//button[@id='product-addtocart-button']");
        $I->wait('3');
        $I->click("//button[@id='product-addtocart-button']");
        $I->waitForElementVisible("//span[@class='counter qty _block-content-loading']//div[@class='loader']");
        $I->waitForElementNotVisible("//span[@class='counter qty _block-content-loading']//div[@class='loader']");
        $I->waitForElementVisible("//span[@class='counter-number']");
        $I->waitForText('1', "15", "//span[@class='counter-number']");
        $I->wait('1');
        $I->click("//a[@class='action showcart']");
        $I->waitForElement("//button[@id='top-cart-btn-checkout']");
        $I->wait('2');
        $I->click("//button[@id='top-cart-btn-checkout']");
        $I->waitForElementVisible("//div[@id='checkout-loader']//div[@class='loader']");
        $I->waitForElementVisible("//body[@class='checkout-index-index page-layout-checkout']//div[@class='loading-mask']//div[@class='loader']");
        $I->waitForElementNotVisible("//div[@id='checkout-loader']//div[@class='loader']");
        $I->waitForElementNotVisible("//body[@class='checkout-index-index page-layout-checkout']//div[@class='loading-mask']//div[@class='loader']");
        $I->waitForElementVisible("//body[@class='checkout-index-index page-layout-checkout']//div[@class='loading-mask']//div[@class='loader']");
        $I->waitForElementNotVisible("//body[@class='checkout-index-index page-layout-checkout']//div[@class='loading-mask']//div[@class='loader']");
        $I->seeCurrentUrlEquals('/checkout/#shipping');
        $I->wait('2');
        $I->waitForElementClickable("//button[@class='button action continue primary']");
        $I->click("//button[@class='button action continue primary']");
        $I->waitForElementVisible("//div[@class='loading-mask']//div[@class='loader']");
        $I->waitForElementNotVisible("//div[@class='loading-mask']//div[@class='loader']");
        $I->seeCurrentUrlEquals('/checkout/#payment');
        $I->wait('2');
        $I->waitForElementClickable("//input[@id='braintree']/parent::div//label");
        $I->click("//input[@id='braintree']/parent::div//label");
        $I->waitForElement("//iframe[@id='braintree-hosted-field-number']");
        $I->wait('1');
        $I->switchToIFrame("//iframe[@id='braintree-hosted-field-number']");
        $I->fillField("//input[@id='credit-card-number']", '4263982640269299');
        $I->switchToIFrame();
        $I->switchToIFrame("//iframe[@id='braintree-hosted-field-expirationMonth']");
        $I->fillField("//input[@id='expiration-month']", '02');
        $I->switchToIFrame();
        $I->switchToIFrame("//iframe[@id='braintree-hosted-field-expirationYear']");
        $I->fillField("//input[@id='expiration-year']", '23');
        $I->switchToIFrame();
        $I->switchToIFrame("//iframe[@id='braintree-hosted-field-cvv']");
        $I->fillField("//input[@id='cvv']", '837');
        $I->switchToIFrame();
        $I->wait('1');
        $I->click("//div[@class='checkout-agreement field choice']//label[@class='label']");
        $I->wait('1');
        $I->click("//button[@class='action primary complete-order _wide']");
        $I->waitForText('Thanks FOR YOUR ORDER, TEST', "15", "//h1[@class='cs-title']");
        $I->seeCurrentUrlEquals('/checkout/onepage/success/');
        $I->wait('1');
    }
}
